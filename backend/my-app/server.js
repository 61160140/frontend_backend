const express = require('express')
const app = express()

app.get('/', (req, res, next) => {
  res.json({ message: 'Hello Nutchaya!!' })
})

app.listen(9000, () => {
  console.log('application is running on port 9000')
})